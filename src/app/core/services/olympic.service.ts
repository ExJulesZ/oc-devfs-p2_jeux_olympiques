import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { Olympic } from '../models/Olympic';
import { Participation } from '../models/Participation';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root',
})
export class OlympicService {
  private olympicUrl = './assets/mock/olympic.json';
  private medalsPerCountry$ = new BehaviorSubject<{name:string,value:number,colorInCharts:string,id:number}[]|undefined>(undefined);
  private countryMedalsPerYear$ = new BehaviorSubject<{name:string,series:{name:string,value:number,athleteCount:number}[],colorInCharts:string,countryName:string,flag:string}[]|undefined>(undefined);

  constructor(private http: HttpClient, private router: Router) {}

  // data loading for the dashboard page and its chart (pie)
  loadInitialDashboardData(): Observable<Olympic[]> {
    return this.http.get<Olympic[]>(this.olympicUrl).pipe(
      tap((value) => {
        let medalsPerCountry:{name:string,value:number,colorInCharts:string,id:number}[] = [];
        value.forEach(elem => {
          let medalsSum = 0;
          let participationsList:Participation[] = elem.participations;
          participationsList.forEach(participation => medalsSum+= participation.medalsCount);
          let name = elem.flag+" "+elem.country
          medalsPerCountry.push({"name":name, "value":medalsSum, "colorInCharts":elem.colorInCharts, "id":elem.id});
        });
        this.medalsPerCountry$.next(medalsPerCountry);
      }),
      catchError((error, caught) => {
        console.error(error);
        // can be useful to end loading state and let the user know something went wrong
        this.medalsPerCountry$.next(undefined);
        return caught;
      })
    );
  }

  // data loading for the details (for a country) page and its chart (line)
  loadInitialDetailsData(countryId:number): Observable<Olympic[]> {
    return this.http.get<Olympic[]>(this.olympicUrl).pipe(
      tap((value) => {
        let data = value.filter(elem => elem.id == countryId)[0];
        if(data){
          let name = data.flag+" "+data.country
          let countryMedalsPerYear:{name:string,series:{name:string,value:number,athleteCount:number}[],colorInCharts:string,countryName:string,flag:string}[] = [
            {"name":name, "series":[], "colorInCharts":data.colorInCharts, "countryName":data.country, "flag":data.flag}
          ];
          let participations:Participation[] = data.participations;
          participations.forEach(elem =>
            countryMedalsPerYear[0].series.push({"name":elem.year+" ("+elem.city+")", "value":elem.medalsCount, "athleteCount":elem.athleteCount})
          );
          this.countryMedalsPerYear$.next(countryMedalsPerYear);
        } else{
          this.router.navigateByUrl('*');
        }
      }),
      catchError((error, caught) => {
        console.error(error);
        // can be useful to end loading state and let the user know something went wrong
        this.countryMedalsPerYear$.next(undefined);
        return caught;
      })
    );
  }

  
  // get data depending on situation and page

  getMedalsPerCountry(): Observable<{name:string,value:number,colorInCharts:string,id:number}[]|undefined> {
    return this.medalsPerCountry$;
  }

  getCountryMedalsPerYear(): Observable<{name:string,series:{name:string,value:number,athleteCount:number}[],colorInCharts:string,countryName:string,flag:string}[]|undefined> {
    return this.countryMedalsPerYear$;
  }

}
