import { Participation } from "./Participation";

export interface Olympic {
    id: number;
    country: string;
    participations: Participation[];
    colorInCharts: string;
    flag: string;
}
