import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { take } from 'rxjs';
import { OlympicService } from 'src/app/core/services/olympic.service';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss']
})
export class DetailsComponent implements OnInit {
  countryId: number = +this.route.snapshot.params['countryId'];

  countryName!: string;
  coutryFlag!: string;

  // informations/stats
  entries!: number;
  totalMedals: number = 0;
  totalAthletes: number = 0;

  data?: any[];

  // chart settings
  view!: [number,number];
  smallView!: [number,number];
  gradient!: boolean;
  showXAxisLabel!: boolean;
  showYAxisLabel!: boolean;
  xAxis!: boolean;
  yAxis!: boolean;
  xAxisLabel!: string;
  yAxisLabel!: string;
  maxXAxisTickLength!: number;
  maxYAxisTickLength!: number;
  colorScheme: {name:string,value:string}[] = [];
  roundDomains!: boolean;
  autoScale!: boolean;

  constructor(private olympicService: OlympicService, private route: ActivatedRoute){}

  ngOnInit() {
    // data loading for the page and its chart
    this.olympicService.loadInitialDetailsData(this.countryId).pipe(take(1)).subscribe();
    this.olympicService.getCountryMedalsPerYear().subscribe(arr => {

      this.data = arr;
      Object.assign(this, this.data);

      // chart colors assignment and informations/stats assignment
      arr?.forEach(elem => {
        this.countryName = elem.countryName;
        this.coutryFlag = elem.flag;

        this.colorScheme.push({name: elem.name, value:elem.colorInCharts});

        this.entries = elem.series.length;
        
        let participations:any[] = elem.series;
        participations.forEach(participation => {
          this.totalMedals+= participation.value;
          this.totalAthletes+= participation.athleteCount;
        });
      });

    });

    // chart settings assignment
    this.view = [700, 300];
    this.smallView = [350, 300];
    this.gradient = true;
    this.showXAxisLabel = true;
    this.showYAxisLabel = true;
    this.xAxis = true;
    this.yAxis = true;
    this.xAxisLabel = "Années de participation";
    this.yAxisLabel = "Médailles";
    this.maxXAxisTickLength = 21;
    this.maxYAxisTickLength = 21;
    this.roundDomains = true;
    this.autoScale = true;
  }

}
