import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { take } from 'rxjs';
import { OlympicService } from 'src/app/core/services/olympic.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  data?: {name:string,value:number,colorInCharts:string,id:number}[];

  // chart settings
  view!: [number,number];
  smallView!: [number,number];
  gradient!: boolean;
  showLabels!: boolean;
  maxLabelLength!: number;
  isDoughnut!: boolean;
  colorScheme: {name:string, value:string}[] = [];

  constructor(private olympicService: OlympicService, private router: Router){}

  ngOnInit() {
    // data loading for the page and its chart
    this.olympicService.loadInitialDashboardData().pipe(take(1)).subscribe();
    this.olympicService.getMedalsPerCountry().subscribe(arr => {
      
      this.data = arr;
      Object.assign(this, this.data);

      // chart colors assignment
      arr?.forEach(elem => {
        this.colorScheme.push({name: elem.name, value:elem.colorInCharts})
      });

    });

    // chart settings assignment
    this.isDoughnut = true;
    this.view = [600, 400];
    this.smallView = [350, 250];
    this.gradient = true;
    this.showLabels = true;
    this.maxLabelLength = 16;
  }

  // on click on a country of the pie chart -> navigate to details page
  clickOnCountry(country:Event): void {
    let countryName = JSON.parse(JSON.stringify(country)).name;
    this.data?.forEach(elem => {
      if(elem.name == countryName){
        this.olympicService.loadInitialDashboardData().pipe(take(1)).subscribe().unsubscribe();
        this.olympicService.getMedalsPerCountry().subscribe().unsubscribe();
        this.router.navigateByUrl('details/'+elem.id);
      }
    });
  }

}
