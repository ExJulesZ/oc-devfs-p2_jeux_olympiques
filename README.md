# P2 : OlympicGames


## Démarrer l’application

1. Récupérer le projet sur Gitlab. Vérifier d'être bien sur la branche main. Cliquer sur "clone" et copier l'adresse HTTPS.

2. A l'emplacement de son choix ouvrer un cmd. Cloner le projet avec 'git clone url_copié'.

3. Entrer dans le projet avec 'cd oc-devfs-p2_jeux_olympiques'.

4. Installer les dépendances en entrant 'npm i'. Et Vérifier ne pas avoir de problèmes de version.

5. Enfin lancer l'application avec 'ng serve'. Attendre la génération puis accéder à localhost:4200.
